import { Theme } from "../../styles/theme";
import { ThemeProvider } from "styled-components";
import { styled } from "styled-components";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import Description from "../../components/Description";
import DescriptionTwo from "../../components/DescriptionTwo";
import SubscriptionPlans from "../../components/SubscriptionPlans";

const Contenair = styled.div`
  @media (max-width: 768px) {
  }
`;
const HomeRoute = () => {
  return (
    <Contenair>
      <ThemeProvider theme={Theme}>
        <Header />
        <Description />
        <SubscriptionPlans />
        <DescriptionTwo />
        <Footer />
      </ThemeProvider>
    </Contenair>
  );
};

export default HomeRoute;
