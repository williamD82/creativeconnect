import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomeRoute from "./Home";
import { AuthProvider } from "../components/AuthProvider";
import RegisterFormRoute from "./RegisterForm";
import LoginFormRoute from "./LoginForm";
import DashBoardRoute from "./Dashboard";
import ProfileFormRoute from "./ProfileForm";
export const Router = () => {
  return (
    <BrowserRouter basename="/">
      <AuthProvider>
        <Routes>
          <Route path="/" element={<HomeRoute />} />
          <Route path="/register" element={<RegisterFormRoute />} />
          <Route path="/login" element={<LoginFormRoute />} />
          <Route path="/dashboard/:uuid" element={<DashBoardRoute />} />
          <Route path="/profileForm/:uuid" element={<ProfileFormRoute />} />
        </Routes>
      </AuthProvider>
    </BrowserRouter>
  );
};

export default Router;
