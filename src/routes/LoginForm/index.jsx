import { Theme } from "../../styles/theme";
import { ThemeProvider } from "styled-components";
import { styled } from "styled-components";
import LoginForm from "../../forms/Login";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
const Contenair = styled.div`
  @media (max-width: 768px) {
  }
`;
const LoginFormRoute = () => {
  return (
    <Contenair>
      <ThemeProvider theme={Theme}>
        <Header />
        <LoginForm />
        <Footer />
      </ThemeProvider>
    </Contenair>
  );
};

export default LoginFormRoute;
