import { Theme } from "../../styles/theme";
import { ThemeProvider } from "styled-components";
import Header from "../../components/Header";
import { styled } from "styled-components";
import Footer from "../../components/Footer";
import Dashboard from "../../components/Dashboard";
const Contenair = styled.div`
  @media (max-width: 768px) {
  }
`;
const DashBoardRoute = () => {
  return (
    <Contenair>
      <ThemeProvider theme={Theme}>
        <Header />
        <Dashboard />
        <Footer />
      </ThemeProvider>
    </Contenair>
  );
};

export default DashBoardRoute;
