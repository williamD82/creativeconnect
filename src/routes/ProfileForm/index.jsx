import { Theme } from "../../styles/theme";
import { ThemeProvider } from "styled-components";
import { styled } from "styled-components";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import ProfileForm from "../../forms/Profile";
const Contenair = styled.div`
  @media (max-width: 768px) {
  }
`;
const ProfileFormRoute = () => {
  return (
    <Contenair>
      <ThemeProvider theme={Theme}>
        <Header />
        <ProfileForm />
        <Footer />
      </ThemeProvider>
    </Contenair>
  );
};

export default ProfileFormRoute;
