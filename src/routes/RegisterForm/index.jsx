import { Theme } from "../../styles/theme";
import { ThemeProvider } from "styled-components";
import { styled } from "styled-components";
import RegisterForm from "../../forms/Register";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
const Contenair = styled.div`
  @media (max-width: 768px) {
  }
`;
const RegisterFormRoute = () => {
  return (
    <Contenair>
      <ThemeProvider theme={Theme}>
        <Header />
        <RegisterForm />
        <Footer />
      </ThemeProvider>
    </Contenair>
  );
};

export default RegisterFormRoute;
