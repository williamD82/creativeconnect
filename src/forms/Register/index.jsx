import React from "react";
import { useForm } from "react-hook-form";
import { createUser } from "../../commons/api/routes/Users";
import { useNavigate } from "react-router-dom";
import { useAuth } from "../../components/AuthProvider";
import Theme from "../../styles/theme";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { styled } from "styled-components";
import { APP_NAME } from "../../commons/constants";

const Contenair = styled.div`
  display: flex;
  justify-content: center;
  margin-top: ${Theme?.margin?.regular * 2}px;
`;

const ContenairForm = styled.form`
  width: 300px;
  border: 2px solid ${Theme?.colors?.primary?.default};
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-top: ${Theme?.margin?.regular * 5}px;
  margin-bottom: ${Theme?.margin?.regular * 5}px;
  border-radius: 5px;
  flex-wrap: wrap;
  align-content: center;
  align-items: flex-start;
`;

const ContenairInput = styled.input`
  min-height: 20px;
  min-width: 90%;
  border: 2px solid ${Theme?.colors?.primary?.default};
  border-radius: 5px;
  margin-bottom: ${Theme?.margin?.regular}px;
`;

const ButtonSignin = styled.button`
  min-height: 40px;
  min-width: 120px;
  border-radius: 5px;
  cursor: pointer;
  border: none;
  margin-top: ${Theme?.margin?.regular}px;
  background-color: ${Theme?.colors?.primary?.default};
  color: ${Theme?.colors?.white};
  &:hover {
    border: 2px solid ${Theme?.colors?.primary?.default};
    background-color: transparent;
    color: ${Theme?.colors?.primary?.default};
  }
`;

const TextLabel = styled.div`
  font-size: ${Theme?.fontSize?.regular}px;
  font-weight: bold;
  margin-right: ${Theme?.margin?.regular / 2}px;
`;

const TextSpan = styled.div`
  font-size: ${Theme?.fontSize?.small}px;
  font-weight: bold;
  color: ${Theme?.colors?.error?.default};
`;
const ContenairTextError = styled.div`
  display: flex;
  align-items: flex-end;
`;
const ContenairTextRegister = styled.div`
  font-size: ${Theme?.fontSize?.xlarge}px;
  font-weight: bold;
  margin-top: ${Theme?.margin?.regular}px;
  margin-bottom: ${Theme?.margin?.regular}px;
`;
const ContenairTextLogin = styled.div`
  font-size: ${Theme?.fontSize?.regular}px;
  font-weight: bold;
  cursor: pointer;
  display: flex;
  align-items: flex-end;
  margin-top: ${Theme?.margin?.regular}px;
  margin-bottom: ${Theme?.margin?.regular}px;
`;
const ContenairTextOnLogin = styled.div`
  font-size: ${Theme?.fontSize?.small}px;
  margin-left: ${Theme?.margin?.regular / 2}px;
`;
export const RegisterForm = () => {
  const { logIn } = useAuth();
  const navigate = useNavigate();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = async (data) => {
    console.log("data Create User", data);
    try {
      const { name, email, password } = data;
      const userData = await createUser(name, email, password);
      console.log("User Data:", userData);
      if (userData && userData.name && userData.email) {
        logIn(userData);
        notifySucces(userData);
        // Pour que le toast s'affiche avant la redirection
        setTimeout(() => {
          navigate("/dashboard");
        }, 2000);
      } else {
        throw new Error("Invalid user data");
      }
    } catch (e) {
      notifyError();
      console.error("Erreur lors de la création du compte", e);
    }
  };

  const notifySucces = (data) => {
    toast.success(`Bienvenue sur ${APP_NAME} ${data?.name}`, {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "colored",
    });
  };
  const notifyError = () => {
    toast.error("Erreur lors de la création du compte", {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "colored",
    });
  };
  return (
    <Contenair>
      <ContenairForm onSubmit={handleSubmit(onSubmit)}>
        <ContenairTextRegister>Inscrivez-vous</ContenairTextRegister>
        <ContenairTextError>
          <TextLabel>Nom</TextLabel>
          {errors?.name && <TextSpan>Ce champs est requis</TextSpan>}
        </ContenairTextError>
        <ContenairInput {...register("name", { required: true })} />
        <ContenairTextError>
          <TextLabel>Mot de passe</TextLabel>
          {errors?.password && <TextSpan>Ce champs est requis</TextSpan>}
        </ContenairTextError>
        <ContenairInput {...register("password", { required: true })} />
        <ContenairTextError>
          <TextLabel>Email</TextLabel>
          {errors?.email && <TextSpan>Ce champs est requis</TextSpan>}
        </ContenairTextError>
        <ContenairInput {...register("email", { required: true })} />
        <ButtonSignin type="submit">S'inscrire</ButtonSignin>
        <ToastContainer />
        <ContenairTextLogin onClick={() => navigate(`/login`)}>
          Déja un compte ?
          <ContenairTextOnLogin>Connectez-vous</ContenairTextOnLogin>
        </ContenairTextLogin>
      </ContenairForm>
    </Contenair>
  );
};

export default RegisterForm;
