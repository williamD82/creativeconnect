import React, { useContext, useState } from "react";
import { useForm } from "react-hook-form";
import { AuthContext } from "../../components/AuthProvider";
import { useNavigate, useParams } from "react-router-dom";
import styled from "styled-components";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Theme from "../../styles/theme";

const Container = styled.div`
  display: flex;
  justify-content: center;
  margin-top: ${Theme?.margin?.regular * 4}px;
`;

const Form = styled.form`
  width: 300px;
`;

const Input = styled.input`
  min-height: 20px;
  min-width: 90%;
  border: 2px solid ${Theme?.colors?.primary?.default};
  border-radius: 5px;
  margin-bottom: ${Theme?.margin?.regular}px;
`;

const TextSpan = styled.span`
  font-size: ${Theme?.fontSize?.small}px;
  font-weight: bold;
  color: ${Theme?.colors?.error?.default};
`;

const Text = styled.div`
  font-size: ${Theme?.fontSize?.regular}px;
  font-weight: bold;
  color: ${Theme?.colors?.secondary?.default};
  margin-bottom: ${Theme?.margin?.regular}px;
`;

const Button = styled.button`
  min-height: 40px;
  min-width: 120px;
  border-radius: 5px;
  cursor: pointer;
  border: none;
  background-color: ${Theme?.colors?.primary?.default};
  color: ${Theme?.colors?.white};
`;

const ProfileForm = () => {
  const { uuid } = useParams();
  const { userData, logIn } = useContext(AuthContext);
  const navigate = useNavigate();

  const [updatedUserData, setUpdatedUserData] = useState(userData);
//   console.log("updatedUserData", updatedUserData);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      name: userData?.name,
      email: userData?.email,
      password: userData?.password,
    },
  });

  const updateUserName = (name, email, password) => {
    const updatedUser = { ...userData, name, email, password };
    setUpdatedUserData(updatedUser);
    logIn(updatedUser);
    notifySuccess(updatedUser);
  };

  const onSubmit = (data) => {
    const { name, email, password } = data;
    updateUserName(name, email, password);
    setTimeout(() => {
      navigate(`/dashboard/${userData?.uuid}`);
    }, 2000);
  };

  const notifySuccess = (data) => {
    toast.success(`Édition du profil réussie pour ${data?.name}`, {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "colored",
    });
  };

  const notifyError = () => {
    toast.error("Erreur lors de l'édition du profil", {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "colored",
    });
  };

  return (
    <Container>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Text>Nom</Text>
        <Input {...register("name", { required: true })} />
        {errors?.name && <TextSpan>Ce champ est requis</TextSpan>}
        <Text>Email</Text>
        <Input {...register("email", { required: true })} />
        {errors?.email && <TextSpan>Ce champ est requis</TextSpan>}
        <Text>Mot de passe</Text>
        <Input {...register("password", { required: true })} />
        {errors?.password && <TextSpan>Ce champ est requis</TextSpan>}
        <Button type="submit">Éditer mon profil</Button>
      </Form>
      <ToastContainer />
    </Container>
  );
};

export default ProfileForm;
