export const BASE_URL = "https://api.airtable.com/v0/appTCqDRYsLondiwH";
export const TOKEN =
  "patSjrlfSyHENVFTY.edb6bad88e2168b95a1a221e11806afe920de1cdb796bb9cad54f27c4d961702";
export const APP_NAME = "Creative Connect";
export const USER_PATH = "Users";
export const USER_PROJETS = "Projects";
export const COLLABORATIONS = "Collaborations";
export const SUBSCRIPTION_PLANS = [
  {
    title: "Découverte",
    description:
      "Accès complet à toutes les fonctionnalités de base de l'application.",
    features: [
      "Connexion avec d'autres créateurs",
      "Possibilité de contribuer à des projets",
    ],
    price: 9.99,
    interval: "mois",
  },
  {
    title: "Créateur",
    description:
      "Accès illimité à toutes les fonctionnalités premium de l'application.",
    features: [
      "Options avancées pour votre profil",
      "Accès prioritaire aux nouveaux projets",
      "Outils de collaboration exclusifs",
    ],
    price: 19.99,
    interval: "mois",
  },
  {
    title: "Entreprise",
    description: "Solution complète pour les équipes ou les organisations.",
    features: [
      "Fonctionnalités personnalisées",
      "Support dédié",
      "Outils de gestion avancés",
    ],
    price: 49.99,
    interval: "mois",
  },
];
