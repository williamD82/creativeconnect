import axios from "axios";
import { BASE_URL, TOKEN, USER_PATH } from "../../../constants/index";
import { v4 as uuidv4 } from "uuid";

export const authenticateUser = async (name, email, password) => {
  try {
    const response = await axios.get(`${BASE_URL}/${USER_PATH}`, {
      headers: {
        Authorization: `Bearer ${TOKEN}`,
      },
      params: {
        filterByFormula: `AND({name} = '${name}', {email} = '${email}',{password} = '${password}')`,
        maxRecords: 1,
      },
    });

    if (response.data.records.length > 0) {
      return { success: true, data: response.data.records[0].fields };
    } else {
      return {
        success: false,
        message: "Informations de connexion incorrectes",
      };
    }
  } catch (error) {
    console.error("Erreur lors de la connexion:", error);
    return {
      success: false,
      message: "Une erreur s'est produite lors de la connexion",
    };
  }
};

export const createUser = async (name, email, password) => {
  try {
    // Générer un UUID aléatoire unique
    const newUuid = `rec-${uuidv4()}`;

    // Créer le nouvel utilisateur avec le nouvel UUID
    const response = await axios.post(
      `${BASE_URL}/${USER_PATH}`,
      {
        records: [
          {
            fields: {
              name: name,
              email: email,
              password: password,
              uuid: newUuid,
            },
          },
        ],
      },
      {
        headers: {
          Authorization: `Bearer ${TOKEN}`,
        },
      }
    );

    console.log("API Response:", response.data);
    if (
      response.data &&
      response.data.records &&
      response.data.records.length > 0
    ) {
      return response.data.records[0].fields;
    } else {
      throw new Error("Invalid response structure");
    }
  } catch (error) {
    console.error("Erreur lors de la création de l'utilisateur:", error);
    throw new Error(
      "Une erreur s'est produite lors de la création de l'utilisateur"
    );
  }
};

export const updateUser = async (name, uuid) => {
  try {
    const response = await axios.patch(
      `${BASE_URL}/${USER_PATH}/${uuid}`,
      {
        fields: {
          name: name,
        },
      },
      {
        headers: {
          Authorization: `Bearer ${TOKEN}`,
        },
      }
    );

    console.log("API Response:", response.data);
    if (response.data && response.data.fields) {
      return response.data.fields;
    } else {
      throw new Error("Invalid response structure");
    }
  } catch (error) {
    console.error("Erreur lors de la mise à jour de l'utilisateur:", error);
    throw new Error("Une erreur s'est produite lors de la mise à jour de l'utilisateur");
  }
};

