const colors = {
  white: "#FFFFFF",
  black: "#000000",
  gray100: "#F8F9FA",
  gray200: "#E9ECEF",
  gray300: "#DEE2E6",
  gray400: "#CED4DA",
  gray500: "#ADB5BD",
  gray600: "#6C757D",
  gray700: "#495057",
  gray800: "#343A40",
  gray900: "#212529",
  primary: {
    default: "#343A40",
    dark: "#212529",
  },
  secondary: {
    default: "#6C757D",
    dark: "#495057",
  },
  accent: {
    default: "#17A2B8",
    dark: "#138496",
  },
  text: {
    default: "#212529",
    light: "#495057",
  },
  error: {
    default: "#DC3545",
  },
  warning: {
    default: "#FFC107",
  },
  success: {
    default: "#28A745",
  },
  disabled: {
    default: "#6C757D",
  },
  boxShadow: {
    default: "0 0 10px 5px #343A40",
  },
  background: {
    default: "#FFFFFF",
    dark: "#343A40",
  },
};

const fontSize = {
  small: 12,
  regular: 14,
  xlarge: 20,
  h1: 67.36,
  h2: 50.56,
  h3: 37.92,
  h4: 28.48,
  h5: 21.28,
};
const padding = {
  regular: 20,
};
const margin = {
  regular: 20,
};
export const Theme = {
  colors,
  fontSize,
  padding,
  margin,
  innerSize: 1170,
  background: colors.white,
  text: colors.black,
  success: colors.green,
  danger: colors.red,
};
export default Theme;
