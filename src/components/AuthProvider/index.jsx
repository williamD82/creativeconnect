import React, { createContext, useContext, useState, useEffect } from "react";

// Création du contexte
export const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [userData, setUserData] = useState(null);

  useEffect(() => {
    // Vérifie si l'utilisateur est déjà connecté lors du chargement de l'application
    const storedUserData = localStorage.getItem("userData");
    if (storedUserData) {
      setUserData(JSON.parse(storedUserData));
      setIsLoggedIn(true);
    }
  }, []);

  const logIn = (data) => {
    setIsLoggedIn(true);
    setUserData(data);
    localStorage.setItem("userData", JSON.stringify(data)); // Enregistre les données utilisateur dans le localStorage
  };

  const logOut = () => {
    setIsLoggedIn(false);
    setUserData(null);
    localStorage.removeItem("userData"); // Supprime les données utilisateur du localStorage
  };

  return (
    <AuthContext.Provider value={{ isLoggedIn, logIn, logOut, userData }}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => {
  const context = useContext(AuthContext);
  if (!context) {
    throw new Error("useAuth must be used within an AuthProvider");
  }
  return context;
};
