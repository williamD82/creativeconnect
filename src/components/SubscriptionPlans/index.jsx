import React, { useContext } from "react";
import CardAbo from "../CardAbo";
import Theme from "../../styles/theme";
import styled from "styled-components";
import { SUBSCRIPTION_PLANS } from "../../commons/constants";
import { AuthContext } from "../../components/AuthProvider";
// import { useNavigate } from "react-router-dom";

const Container = styled.div`
  background: ${Theme?.colors?.primary?.default};
  @media (max-width: 768px) {
    // Add styles for responsiveness if needed
  }
`;

const ContainerPlans = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-evenly;
  @media (max-width: 768px) {
    flex-direction: column;
    align-content: center;
  }
`;

const ContainerText = styled.div`
  font-size: ${Theme?.fontSize?.h2}px;
  color: white;
  text-align: center;
`;

const SubscriptionPlans = () => {
  const { userData } = useContext(AuthContext);
  const hasSubscription = userData?.subscriptionPlans?.length > 0;

  return (
    <Container>
      {hasSubscription ? (
        <ContainerText></ContainerText>
      ) : (
        <>
          <ContainerText>Plans</ContainerText>
          <ContainerPlans>
            {SUBSCRIPTION_PLANS.map((plan) => (
              <CardAbo
                key={plan.title}
                title={plan.title}
                description={plan.description}
                price={plan.price}
                features={plan.features}
                interval={plan.interval}
              />
            ))}
          </ContainerPlans>
        </>
      )}
    </Container>
  );
};

export default SubscriptionPlans;
