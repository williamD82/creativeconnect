import React from "react";
import styled from "styled-components";
import Theme from "../../styles/theme";

const Contenair = styled.div`
  height: 380px;
  width: 290px;
  border: 2px solid ${Theme?.colors?.primary?.default};
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: space-evenly;
  margin-top: ${Theme?.margin?.regular * 3}px;
  margin-bottom: ${Theme?.margin?.regular * 3}px;
  background: white;
  border-radius: 5px;
  overflow: hidden;
  @media (max-width: 768px) {
    width: 90%;
    margin-bottom: ${Theme?.margin?.regular * 3}px;
  }
`;

const ContenairTitlePlans = styled.div`
  font-size: ${Theme?.fontSize?.xlarge}px;
  font-weight: bold;
  margin-top: 5px;
  margin-bottom: 5px;
`;

const ContenairText = styled.div`
  font-size: ${Theme?.fontSize?.regular}px;
  margin: ${Theme?.margin?.regular}px;
  text-align: center;
`;
const BtnChoice = styled.button`
  min-height: 40px;
  min-width: 120px;
  border-radius: 5px;
  cursor: pointer;
  border: none;
  background-color: ${Theme?.colors?.primary?.default};
  color: ${Theme?.colors?.white};
  &:hover {
    border: 2px solid ${Theme?.colors?.primary?.default};
    background-color: transparent;
    color: ${Theme?.colors?.primary?.default};
  }
  @media (max-width: 768px) {
    margin-top: ${Theme?.margin?.regular * 4}px;
  }
`;
const CardBook = ({ title, description, price, interval, features }) => {
  return (
    <Contenair>
      <ContenairTitlePlans>{title}</ContenairTitlePlans>
      <ContenairText>{description}</ContenairText>
      <ContenairText>{features}</ContenairText>
      <ContenairText>
        {price} € / {interval}
      </ContenairText>
      <BtnChoice>Choisir</BtnChoice>
    </Contenair>
  );
};

export default CardBook;
