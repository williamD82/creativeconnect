import { APP_NAME } from "../../commons/constants";
import { FaFacebook, FaInstagram } from "react-icons/fa";
import { MdOutlineCopyright } from "react-icons/md";
import Theme from "../../styles/theme";
import { styled } from "styled-components";
const Contenair = styled.div`
  color: white;
  height: 250px;
  display: flex;
  align-items: center;
  justify-content: space-around;
  margin-top: ${Theme?.margin?.regular}px;
  background: ${Theme?.colors?.primary?.default};
  @media (max-width: 768px) {
    color: white;
    height: 250px;
    display: flex;
    align-items: center;
    justify-content: space-around;
    margin-top: ${Theme?.margin?.regular}px;
    background: ${Theme?.colors?.primary?.default};
    flex-direction: column;
  }
`;
const ContenairBigTitleBy = styled.div`
  @media (max-width: 768px) {
  }
`;
const TextBigTitle = styled.div`
  font-size: ${Theme?.fontSize?.h3}px;
  font-weight: bold;
  display: flex;
  @media (max-width: 768px) {
  }
`;

const TextBy = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: ${Theme?.fontSize?.small}px;
`;
const TextTitle = styled.div`
  font-size: ${Theme?.fontSize?.xlarge}px;
  font-weight: bold;
  margin-bottom: 20px;
  @media (max-width: 768px) {
  }
`;
const ContenairTitleLink = styled.div`
  @media (max-width: 768px) {
  }
`;
const ContenairLink = styled.div`
  display: flex;
  justify-content: space-around;
  flex-wrap: wrap;
  @media (max-width: 768px) {
  }
`;
const Footer = () => {
  return (
    <Contenair>
      <ContenairBigTitleBy>
        <TextBigTitle>{APP_NAME}</TextBigTitle>
        <TextBy>
          <MdOutlineCopyright />
          William Debezis 2024
        </TextBy>
      </ContenairBigTitleBy>
      <ContenairTitleLink>
        <TextTitle>Suivez-nous</TextTitle>
        <ContenairLink>
          <FaFacebook size={40} />
          <FaInstagram size={40} />
        </ContenairLink>
      </ContenairTitleLink>
    </Contenair>
  );
};

export default Footer;
