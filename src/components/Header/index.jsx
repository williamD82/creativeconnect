import { useContext } from "react";
import { APP_NAME } from "../../commons/constants/index";
import Theme from "../../styles/theme";
import { AiFillHome } from "react-icons/ai";
import { useNavigate } from "react-router-dom";
import { AuthContext } from "../../components/AuthProvider";
import { styled } from "styled-components";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import noPicture from "../../assets/images/noPicture.jpg";
const Contenair = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-around;
  background-color: ${Theme?.colors?.white};
  height: 80px;
  @media (max-width: 768px) {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    display: flex;
    align-items: center;
    background-color: ${Theme?.colors?.white};
    height: 100px;
    flex-wrap: wrap;
    justify-content: center;
  }
`;

const ContenairBtn = styled.div`
  @media (max-width: 768px) {
  }
`;

const ContenairTitleIcon = styled.div`
  font-size: ${Theme?.fontSize?.h3}px;
  color: ${Theme?.colors?.primary?.default};
  display: flex;
`;

const Title = styled.div`
  margin-right: ${Theme?.margin?.regular};
`;

const ButtonLogin = styled.button`
  min-height: 40px;
  min-width: 120px;
  border-radius: 5px;
  cursor: pointer;
  border: 2px solid ${Theme?.colors?.primary?.default};
  background-color: transparent;
  color: ${Theme?.colors?.primary?.default};
  margin-right: ${Theme?.margin?.regular}px;
  &:hover {
    background-color: ${Theme?.colors?.primary?.default};
    color: ${Theme?.colors?.white};
  }
`;

const ButtonSignin = styled.button`
  min-height: 40px;
  min-width: 120px;
  border-radius: 5px;
  cursor: pointer;
  border: none;
  background-color: ${Theme?.colors?.primary?.default};
  color: ${Theme?.colors?.white};
  &:hover {
    border: 2px solid ${Theme?.colors?.primary?.default};
    background-color: transparent;
    color: ${Theme?.colors?.primary?.default};
  }
`;
const ContenairBtnLogOutlastName = styled.div`
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  justify-content: center;
  @media (max-width: 768px) {
    background-color: white;
  }
`;
const ContenairHello = styled.div`
  font-size: ${Theme?.fontSize?.regular}px;
  color: ${Theme?.colors?.primary?.default};
  font-weight: bold;
  @media (max-width: 768px) {
  }
`;
const ContenairSubscriptionPlans = styled.div`
  font-size: ${Theme?.fontSize?.regular}px;
  color: ${Theme?.colors?.primary?.default};
  margin-right: 20px;
  font-weight: bold;
  @media (max-width: 768px) {
  }
`;
const ContenairHelloImg = styled.img`
  height: 40px;
  width: 40px;
  border-radius: 90%;
  margin-left: 10px;
  margin-right: 10px;
  border: 2px solid ${Theme?.colors?.primary?.default};
  @media (max-width: 768px) {
  }
`;
const ContenairHomeIcon = styled.div`
  margin-right: ${Theme?.margin?.regular * 4}px;
  color: ${Theme?.colors?.secondary?.default};
  border: 2px solid ${Theme?.colors?.primary?.default};
  border-radius: 50%;
  cursor: pointer;
  height: 40px;
  width: 40px;
  @media (max-width: 768px) {
  }
`;
export const Header = () => {
  const navigate = useNavigate();
  const { isLoggedIn, userData } = useContext(AuthContext);
  const { logOut } = useContext(AuthContext);
  const imageSrc = userData?.image?.[0]?.url;

  const handleLogOut = () => {
    notifyError();
    logOut();
    setTimeout(() => {
      navigate("/");
    }, 2500);
  };

  const notifyError = () => {
    toast.error(`A bientôt ${userData?.name}`, {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "colored",
    });
  };

  return (
    <Contenair>
      <ContenairTitleIcon>
        {isLoggedIn ? (
          <ContenairHomeIcon>
            <AiFillHome onClick={() => navigate(`/`)} />
          </ContenairHomeIcon>
        ) : (
          <></>
        )}
        <Title>{APP_NAME}</Title>
      </ContenairTitleIcon>
      <ContenairBtn>
        {isLoggedIn && userData ? (
          <ContenairBtnLogOutlastName>
            <ContenairHello>Bonjour {userData?.name}</ContenairHello>
            <ContenairHelloImg src={imageSrc || noPicture} alt="" />
            <ContenairSubscriptionPlans>
              {userData?.subscriptionPlans}
            </ContenairSubscriptionPlans>
            <ButtonSignin onClick={handleLogOut}>Déconnexion</ButtonSignin>
          </ContenairBtnLogOutlastName>
        ) : (
          <>
            <ButtonLogin onClick={() => navigate(`/login`)}>
              Se connecter
            </ButtonLogin>
            <ButtonSignin onClick={() => navigate(`/register`)}>
              S'inscrire
            </ButtonSignin>
          </>
        )}
      </ContenairBtn>
      <ToastContainer />
    </Contenair>
  );
};

export default Header;
