import React from "react";
import styled from "styled-components";
import Theme from "../../styles/theme";
import noPictureProject from "../../assets/images/noPictureProject.png";

const Contenair = styled.div`
  height: 250px;
  width: 190px;
  border: 2px solid ${Theme?.colors?.primary?.default};
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: space-evenly;
  margin-top: ${Theme?.margin?.regular * 3}px;
  background: white;
  border-radius: 5px;
  overflow: hidden;
`;

const ImgProject = styled.img`
  height: 100%;
  width: 100%;
  object-fit: cover;
  border-radius: 5px;
  transition: transform 0.3s, box-shadow 0.3s;
  &:hover {
    box-shadow: 0 0 10px 5px ${Theme?.colors?.primary?.default};
    cursor: pointer;
    transform: scale(1.05);
  }
`;

const ImgNoProject = styled.img`
  height: 150px;
  width: auto;
  border-radius: 5px;
`;

const ContenairTitleProject = styled.div`
  font-size: ${Theme?.fontSize?.small}px;
  font-weight: bold;
  margin-top: 5px;
  margin-bottom: 5px;
`;

const ContenairAuthor = styled.div`
  font-size: ${Theme?.fontSize?.small}px;
`;

const CardBook = ({ title, description, status, image, team }) => {
  return (
    <Contenair>
      {image ? (
        <ImgProject src={image} alt="" />
      ) : (
        <ImgNoProject src={noPictureProject} alt="" />
      )}
      <ContenairTitleProject>{title}</ContenairTitleProject>
      <ContenairAuthor>{description}</ContenairAuthor>
      <ContenairAuthor>{status}</ContenairAuthor>
      <ContenairAuthor>{team}</ContenairAuthor>
    </Contenair>
  );
};

export default CardBook;
