import React, { useContext } from "react";
import { AuthContext } from "../../components/AuthProvider";
import CardProject from "../CardProject";
import Theme from "../../styles/theme";
import { useNavigate } from "react-router-dom";
import { styled } from "styled-components";
import noPicture from "../../assets/images/noPicture.jpg";

const Contenair = styled.div`
  margin-top: ${Theme?.margin?.regular * 4}px;
  @media (max-width: 768px) {
  }
`;
const ContenairSubTitleProject = styled.div`
  margin-bottom: ${Theme?.margin?.regular * 4}px;
  @media (max-width: 768px) {
  }
`;
const TextTitle = styled.div`
  text-align: center;
  max-width: 100%;
  max-height: 100%;
  margin-bottom: ${Theme?.margin?.regular * 4}px;
  font-size: ${Theme?.fontSize?.h2}px;
  font-weight: bold;
  @media (max-width: 768px) {
    margin-bottom: ${Theme?.margin?.regular}px;
    margin-top: ${Theme?.margin?.regular * 6}px;
  }
`;
const TextSubTitle = styled.div`
  font-size: ${Theme?.fontSize?.h5}px;
  font-weight: bold;
  color: ${Theme?.colors?.primary?.default};
  margin-left: ${Theme?.margin?.regular}px;
`;
const TextSubSubTitle = styled.div`
  font-size: ${Theme?.fontSize?.regular}px;
  font-weight: bold;
  color: ${Theme?.colors?.secondary?.default};
  margin-bottom: ${Theme?.margin?.regular}px;
  @media (max-width: 768px) {
  }
`;
const ImgDashboard = styled.img`
  height: 120px;
  margin-bottom: ${Theme?.margin?.regular}px;
  border: 2px solid ${Theme?.colors?.secondary?.default};
  border-radius: 15px;
`;
const ContenairProject = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-evenly;
  @media (max-width: 768px) {
    display: flex;
    flex-wrap: wrap;
    flex-direction: column;
    align-content: center;
  }
`;
const ContenairProfil = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  margin-top: ${Theme?.margin?.regular * 2}px;
  margin-bottom: ${Theme?.margin?.regular * 4}px;
  @media (max-width: 768px) {
    display: flex;
    flex-wrap: wrap;
    margin-top: ${Theme?.margin?.regular}px;
  }
`;
const Divider = styled.div`
  height: 2px;
  margin-bottom: ${Theme?.margin?.regular * 4}px;
  margin-left: ${Theme?.margin?.regular * 4}px;
  margin-right: ${Theme?.margin?.regular * 4}px;
  background: ${Theme?.colors?.secondary?.default};
  @media (max-width: 768px) {
    height: 2px;
    margin-bottom: ${Theme?.margin?.regular * 4}px;
    background: ${Theme?.colors?.secondary?.default};
  }
`;
const BtnAddProject = styled.button`
  min-height: 40px;
  min-width: 120px;
  border-radius: 5px;
  cursor: pointer;
  border: none;
  background-color: ${Theme?.colors?.primary?.default};
  color: ${Theme?.colors?.white};
  &:hover {
    border: 2px solid ${Theme?.colors?.primary?.default};
    background-color: transparent;
    color: ${Theme?.colors?.primary?.default};
  }
  @media (max-width: 768px) {
    margin-top: ${Theme?.margin?.regular * 4}px;
  }
`;
const BtnEditProfil = styled.button`
  min-height: 40px;
  min-width: 120px;
  border-radius: 5px;
  cursor: pointer;
  border: none;
  background-color: ${Theme?.colors?.primary?.default};
  color: ${Theme?.colors?.white};
  &:hover {
    border: 2px solid ${Theme?.colors?.primary?.default};
    background-color: transparent;
    color: ${Theme?.colors?.primary?.default};
  }
  @media (max-width: 768px) {
  }
`;
const Dashboard = () => {
  const { userData } = useContext(AuthContext);
  const navigate = useNavigate();
  // console.log("userData", userData);
  const hasProjects = userData?.["title (from nameProject)"]?.length > 0;
  const hasProjectsCollaboration = userData?.Collaborations?.length > 0;

  return (
    <Contenair>
      <TextTitle>Dashboard</TextTitle>
      <TextSubTitle>Mon profil</TextSubTitle>
      <ContenairProfil>
        <ImgDashboard src={userData?.image?.[0]?.url ?? noPicture} />
        <TextSubSubTitle>{userData?.name}</TextSubSubTitle>
        <TextSubSubTitle>Email: {userData?.email}</TextSubSubTitle>
        <TextSubSubTitle>Password: {userData?.password}</TextSubSubTitle>
        <BtnEditProfil
          onClick={() => {
            navigate(`/profileForm/${userData?.uuid}`);
          }}
        >
          Modifier mon profil
        </BtnEditProfil>
      </ContenairProfil>
      <Divider></Divider>
      <ContenairSubTitleProject>
        <TextSubTitle>{hasProjects ? <>Projets crées</> : <></>}</TextSubTitle>
        <ContenairProject>
          {hasProjects ? (
            <CardProject
              title={userData?.["title (from nameProject)"]}
              description={userData?.["description (from descriptionPtoject)"]}
              image={userData?.["image (from imageProject)"][0]?.url}
              team={userData?.[
                "name (from teamMembers) (from TeamProject)"
              ]?.join(" - ")}
              status={userData?.["status (from StatusProject)"]}
            />
          ) : (
            <p>Pas de projet crée</p>
          )}
          <BtnAddProject onClick={() => navigate(`/addBookDeposed`)}>
            Ajouter un projet
          </BtnAddProject>
        </ContenairProject>
      </ContenairSubTitleProject>
      <Divider></Divider>
      <ContenairSubTitleProject>
        <TextSubTitle>
          {hasProjectsCollaboration ? <>Projets en collaboration</> : <></>}
        </TextSubTitle>
        <ContenairProject>
          {hasProjectsCollaboration ? (
            <CardProject
              title={userData?.["title (from nameProject)"]}
              description={userData?.["description (from descriptionPtoject)"]}
              image={userData?.["image (from imageProject)"][0]?.url}
              team={userData?.[
                "name (from teamMembers) (from TeamProject)"
              ]?.join(" - ")}
              status={userData?.["status (from StatusProject)"]}
            />
          ) : (
            <p>Pas de projet en collaboration</p>
          )}
        </ContenairProject>
      </ContenairSubTitleProject>
    </Contenair>
  );
};

export default Dashboard;
